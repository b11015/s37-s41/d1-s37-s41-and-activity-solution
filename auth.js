// [SECTION] DEPENDENCIES
const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI";

/*
  Notes:

	JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow acces to certain parts of our app.

	JWT is like a gift wrapping service that is able to encode our user details which can only be unwrapped by jwt's own methods and if the secret provided is intact.

	IF the JWT seemed tampered with, we will reject the users attempt to access a feature

*/


module.exports.createAccessToken = (user) => {
    console.log(user);

    // data object is created to contain some details of user
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }
    console.log(data);

    return jwt.sign(data, secret, {})
}

// NOTES:
/*
  1. You can only get access token when a user log in in your app with the correct credentials.

  2. As a user, you can only get your own details from your own token from logging in.

  3. JWT is not meant for sensitive data.

  4. JWT is like a passport, you use around the app to access certain features meant for your type of user.
*/

// goal : to check if the token exist or it  is not tampered

module.exports.verify = (req, res, next) => {
    // req.headers.authorization - contains jwt / token
    let token = req.headers.authorization;

    if (typeof token === "undefined") {

        // typeof result is a string
        return res.send({ auth: "Failed. No Token" })

    } else {
        console.log(token);

        /*
          slice method is used for arrays and strings
          default
            Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTcwMDI2OTIyNDE4OTAzNTQ3NGRmOCIsImVtYWlsIjoiY2FsbE1lTGlzYUBnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjU0MTMyMTE0fQ.gHYGG00r3vxUDjCOKV-If4AARuPTFMsvLrcPc4JmIZg
        */

        token = token.slice(7, token.length)
        console.log(token)

        /*
          expected output:

          eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTcwMDI2OTIyNDE4OTAzNTQ3NGRmOCIsImVtYWlsIjoiY2FsbE1lTGlzYUBnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjU0MTMyMTE0fQ.gHYGG00r3vxUDjCOKV-If4AARuPTFMsvLrcPc4JmIZg

        */

        jwt.verify(token, secret, (err, decodedToken) => {
            if (err) {
                return res.send({
                    auth: "Failed",
                    message: err.message
                })
            } else {
                req.user = decodedToken
                    // will let us proceed to the next middleware or controller
                next();
            }
        })
    }
};

// verifying an admin

module.exports.verifyAdmin = (req, res, next) => {

    if (req.user.isAdmin) {
        next();
    } else {
        return res.send({
            auth: "Failed",
            message: "Action Forbidden"
        })
    }
};