const express = require('express');
const router = express.Router();

// [SECTION] IMPORTED MODULES
const courseControllers = require('../controllers/courseControllers');

console.log(courseControllers);

const auth = require('../auth')

const { verify, verifyAdmin } = auth;

// created / add course
router.post('/', verify, verifyAdmin, courseControllers.addCourse)

// get all courses
router.get('/', courseControllers.getAllCourses)

// ACTIVITY SOLUTION #3
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse)

// update a course
router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse)

// ACTIVITY SOLUTION #4

router.put('/archive/:id', verify, verifyAdmin, courseControllers.archiveCourse)

router.put('/activate/:id', verify, verifyAdmin, courseControllers.activateCourse)

router.get('/getActiveCourses', courseControllers.getActiveCourse);

// find courses by name retrieval with course.body
router.post('/findCoursesByName', courseControllers.findCoursesByName);

module.exports = router;