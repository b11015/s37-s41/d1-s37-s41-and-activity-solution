// [SECTION] DEPENDENCIES
const express = require('express');
const router = express.Router();

// [SECTION] IMPORTED MODULES
const userControllers = require('../controllers/userControllers');

const auth = require('../auth');

// object destructuring from auth module
// this is the middleware
const { verify, verifyAdmin } = auth;

console.log(userControllers);


// [SECTION] ROUTES

// register user
router.post('/', userControllers.registerUser);

router.get('/', userControllers.getAllUsers);

// login user
router.post('/login', userControllers.loginUser);

// get user details
router.get('/getUserDetails', verify, userControllers.getUserDetails);

// ACTIVITY SOULTION #3
router.post('/checkEmailExists', userControllers.checkEmailExists);

// UPDATING USER DETAILS
router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);

router.post('/enroll', verify, userControllers.enroll);

router.get('/getEnrollments', verify, userControllers.getEnrollments)

module.exports = router;