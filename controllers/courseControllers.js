const Course = require('../models/Course');
const User = require('../models/User');

module.exports.addCourse = (req, res) => {
    console.log(req.body);
    Course.find({})

    let newCourse = new Course({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    })

    newCourse.save()
        .then(result => res.send(result))
        .catch(err => res.send(err));
};

// [SECTION] GET ALL COURSE
module.exports.getAllCourses = (req, res) => {
    Course.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err));
};

// ACTIVITY SOLUTION #3

module.exports.getSingleCourse = (req, res) => {
    console.log(req.params);
    Course.findById(req.params.id)
        .then(result => res.send(result))
        .catch(err => res.send(err))
};

// [SECTION] UPDATE COURSE

module.exports.updateCourse = (req, res) => {

    let updates = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price

    }

    Course.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then(updatedCourse => res.send(updatedCourse))
        .catch(err => res.send(err))
};

// ACTIVITY SOLUTION #4

module.exports.archiveCourse = (req, res) => {

    let updates = {

        isActive: false
    }

    Course.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then(archiveCourse => res.send(archiveCourse))
        .catch(err => res.send(err))
};

module.exports.activateCourse = (req, res) => {

    let updates = {

        isActive: true
    }

    Course.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then(activateCourse => res.send(activateCourse))
        .catch(err => res.send(err))
};

module.exports.getActiveCourse = (req, res) => {
    Course.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err))
};

// [SECTION] FIND COURSES BY NAME

module.exports.findCoursesByName = (req, res) => {
    console.log(req.body) // contain the name of the course you are looking for

    Course.find({ name: { $regex: req.body.name, $options: '$i' } })
        .then(result => {
            if (result.length === 0) {
                return res.send('No courses found')
            } else {
                return res.send(result)
            }
        })
};